# Test: the maze.
 
## What you have to do:
- clone this repository  
- branch locally from the develop branch to make a local "feature/maze-solution-[your-name]" branch  
- on your local feature/maze-solution-[your-name] branch, execute the tasks described in the Tasks section below  
- commit, push to origin when appropriate.  
- when you're done, make a pull request from the branch you pushed to the develop branch  
- as long as the described tasks are completed, you can modify the code as you see fit. Some incomplete methods can be given as hints but you're not forced to use them.  
  
## Initial state

The files on which you'll have to work live in the maze-test folder. The content of the maze-builder folder will be used in one specific task, where its utility is explained.
If you open the index.html file of the maze-test folder with a browser, you should see a maze, with a row of 5 buttons on top, labelled left, right, up, down and solve.  
Clicking on those shouldn't do anything.  
Take the time to look inside index.html, app.js, maze.js and solver.js before starting the tasks.

## Tasks

1- "Make it move": fill existing methods and/or add any additional code required so that clicking on the buttons labelled "left", "right", "up" and "down" actually move the character in the maze, respecting the following requirements:  
- the character can't go outside of the maze  
- the character can't go on a wall  
You can also optionally map the movements to the arrow keys of the keyboard.  
[evaluates: base javascript knowledge, understanding existing code]  
  
2- "In the end": Modify the code so that a message is displayed when the character is moved on the endpoint. The message should contain the number of valid moves made to reach the point, and give a (working) option to reset the maze.  
[evaluates: base javascript knowledge, understanding existing code, application flow and eventing]  
  
3- "Give me more choices": Modify the code so that before rendering, the "app" offers a choice between different mazes.
To generate mazes, you can use the code inside of the maze-builder folder. If you open builder.html in your browser, you'll be able to build a maze with specified number of rows and columns. You can then click on the different cells to toggle between floor and wall. When done, a click on "export" will alert the array representation of the maze.
Don't forget to add beginning and ending points before using your maze ;)
[evaluates: base javascript knowledge, understanding existing code, application flow]  
  
4- "Show me the way": Implement the MazeSolver object. Intanciated with a loaded maze, the MazeSolver object should be able to calculate a path (as an array of positions) from the current character position to the endpoint, and to move the character step by step following this calculated path. When done, bind the solve button to the action of calculating and moving the character in an observable way (no teleportation...).  
[evaluates: algorithms and data structures]  
  
5- "Different rendering": In the current code, the Maze object is responsible for keeping, validating and updating internal state as well as perform the rendering to html tasks. It may be problematic, e.g. if we want to render to be able to render the maze to a canvas as well as to the DOM, or don't render at all when performing testing.  
Refactor the current code as you think is the most appropriate to decouple internal state management from rendering.  
[evaluates: refactoring, architecting an app]  
  
6- "Whatever": [optional] If you see some more improvements that you can and want to perform, go ahead!