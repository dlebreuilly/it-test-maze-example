var root = document.getElementById("maze");
var gebid = document.getElementById.bind(document);

function clear(){
	while (root.firstChild) {
		root.removeChild(root.firstChild);
	}
}

function build(){
	clear();
	var rows = parseInt(gebid("rows").value, 10);
	var cols = parseInt(gebid("cols").value, 10);
	if(rows > 0 && cols > 0){
		for(var i=0;i<rows;i++){
			var row = document.createElement("div");
			row.className = "row";
			for (var j=0;j<cols;j++){
				var cell = document.createElement("div");
				cell.className = "cell";
				row.appendChild(cell);
			}
			root.appendChild(row);
		}
		bindToggleEvents();
	}
}

function bindToggleEvents(){
	var cells = Array.prototype.slice.call(root.querySelectorAll(".cell"));
	cells.forEach(function(el){
		el.addEventListener('click', toggleType);
	});
	
}

function toggleType(e){
	if(this.className.indexOf("wall") != -1){
		this.className = this.className.replace("wall", '');
	}
	else{
		this.className = this.className + " wall";
	}
}

function exportResult(){
	var rows = Array.prototype.slice.call(root.children);
	var result = [];
	rows.reduce(function(acc, el){
		var row = [];
		var cells = Array.prototype.slice.call(el.children);
		cells.reduce(function(acc2, el2){
			var type = el2.className.indexOf("wall") != -1 ? 2 : 1;
			acc2.push(type);
			return acc2;
		}, row)
		acc.push(row);
		return acc;
	}, result);
	alert(JSON.stringify(result));
}

gebid("build").addEventListener("click", build);
gebid("export").addEventListener("click", exportResult);
