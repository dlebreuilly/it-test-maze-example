var app = window.app || {};
/*this property contains array-like representations of mazes. 
a representation is in the form of a 2-dimensions array.
first level is used as the row number (so, the y coordinate), second level as the column number (x coordinate)
The elements at the second level are integers representing the type of the cell: 0: starting point, 1: floor, 2: wall, 3: endpoint
mazes are stored directly in the app instead of in external files or databases because this example is made to run without webserver, and chrome/IE don't allow ajax queries on file:// protocol. 
*/
app.mazes = {
	"maze1" : [[2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2],[0,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,1,1,1,2,1,2,1,2],[2,1,2,2,2,1,2,2,2,2,1,2,1,2,1,2,1,2,2,1,2,1,2,1,2],[2,1,1,1,2,1,2,1,1,2,2,2,2,2,1,2,1,2,2,1,2,1,2,1,2],[2,1,2,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,2],[2,1,2,1,2,1,2,2,2,2,2,1,2,2,2,2,2,1,2,1,2,1,2,1,2],[2,1,2,1,2,1,1,1,1,1,2,1,2,1,1,1,2,1,2,1,2,1,2,1,2],[2,1,2,1,2,2,2,2,2,2,2,1,2,2,2,2,2,1,2,1,2,1,2,1,2],[2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,1,1,2],[2,1,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,1,2,1,2],[2,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,2],[2,1,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,1,2,1,2],[2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2],[2,1,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,1,2],[2,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2],[2,1,2,1,2,1,2,2,2,1,2,2,2,2,2,2,2,2,2,2,1,2,2,1,2],[2,1,2,1,2,2,2,1,2,1,2,1,1,1,1,1,1,1,1,2,1,2,1,1,2],[2,2,2,1,1,1,2,1,2,1,2,1,2,2,2,2,2,2,1,2,1,2,1,1,2],[2,1,1,1,2,2,2,1,2,1,2,1,2,1,1,1,1,2,1,2,1,2,1,1,2],[2,1,2,2,1,2,1,1,2,1,2,2,2,2,1,2,2,2,1,2,1,2,1,1,2],[2,1,1,2,1,1,1,1,1,1,1,1,1,1,1,2,2,2,1,2,1,2,2,2,2],[2,1,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,1,1,1,1,3],[2,1,2,1,2,2,1,2,1,1,1,2,1,1,1,2,1,1,2,2,2,1,2,2,2],[2,1,1,1,1,1,1,1,1,2,1,1,1,2,1,1,1,1,1,1,2,1,1,1,2],[2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]]
};

/*initialization of the app: maze loading and events binding*/
app.init = function(){
	app.maze = new Maze(document.getElementById("maze")); //inits a maze object
	app.maze.loadMaze(app.mazes.maze1, app.bindControls.bind(app)); //loads a representation in the maze object, calls event binding code as callback after loading
	app.maze.onSolved = function(){return;}; //maybe do something if the character reaches the end of the maze?
	app.solver = new MazeSolver(maze); //a solver object may be used by the "solve" button...
};

/*called to map the buttons to the maze/solver actions*/
app.bindControls = function(){
	/*may be the good place to bind some maze methods to front buttons*/
};

/*let's do the work!*/
app.init();