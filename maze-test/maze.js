/*represents a Maze component.
Is responsible for drawing the maze and handling all the maze internal state.
It must be given an html block element at construction, in which it will render the maze.
The drawing of the maze and initial set up are made when calling the loadMaze method.
*/
function Maze(rootElem){
/*the internal representation of the maze is in the form of a 2-dimensions array.
First level is used as the row number (so, the y coordinate), second level as the column number (x coordinate)
The elements at the second level are integers representing the type of the cell: 0: starting point, 1: floor, 2: wall, 3: endpoint
The origin (0, 0) is the top-left corner of the maze. Y-axis values increase when going to the bottom, X-axis values increase when going to the right.*/
	this._representation = null;
/* position of the character in the maze. this should be an object of the form {x: integer, y: integer}*/
	this._characterPos = null;
/* the point where the character should go to win. this should be an object of the form {x: integer, y: integer}*/
	this.endPoint = null;
/* the point where the character starts the maze. this should be an object of the form {x: integer, y: integer}*/
	this.beginningPoint = null;
/* the html element that is used as root to render the maze*/
	this._rootElem = rootElem;
	
/*the mins and maxs for the two axis, just for convenience*/
	this.maxX = 0;
	this.maxY = 0;
	this.minX = 0;
	this.minY = 0;
	
/*can be overloaded by calling code to do something when the character reaches the end of the maze*/
	this.onSolved = function(){return;};
};

/*loads the representation of the maze into the object, triggers the _onLoad method that does the initialization of this maze object from the representation, and executes the given callback (if provided) when done*/
Maze.prototype.loadMaze = function(representation, callback){
	this._representation = representation;
	this._onLoad();
	if(typeof(callback) === "function"){
		callback();
	}
};

/*called by the loadMaze method after having loaded the internal representation of the maze in memory.
sets up the properties that depends on the representation and render the maze.
*/
Maze.prototype._onLoad = function(){
	if(this._representation){
		//set up the max boundaries
		this.maxY = (this._representation.length - 1);
		if(this.maxY > 0 && this._representation[0]){
			this.maxX = (this._representation[0].length - 1);
			//clears previous maze if any
			this._clearMazeRoot();
			//render maze to html from the internal representation. Sets the beginning and ending points.
			this._initAndRenderMaze();
			//sets the character position at the beginning point
			this._initCharacter();
			//manipulates the html to display the character
			this._renderCharacter();
		}
	}
};

/*Goes through the internal representation of the maze and render it as html inside of the root element provided at the construction of the Maze object.
maze is rendered as a minimalistic serie of div elements following closely the structure of the representation. (see comment inside of the constructor for more information about the internal representation).
A first level of div represent a maze row, containing maze cells. Cells' roles are indicated by css classes.
Rows containers have a "row" class, all cells have a "cell" class. The walls get an additional "wall" class and the endpoint an additional "end" class.
All the rest can then be handled via css.
As an example, the following 3*3 (useless) maze: [[2,2,2],[2,0,3][2,2,2]]
Would be rendred inside the root element as:
<div class="row">
	<div class="cell wall"></div>
	<div class="cell wall"></div>
	<div class="cell wall"></div>
</div>
<div class="row">
	<div class="cell wall"></div>
	<div class="cell"></div>
	<div class="cell end"></div>
</div>
<div class="row">
	<div class="cell wall"></div>
	<div class="cell wall"></div>
	<div class="cell wall"></div>
</div>
*/
Maze.prototype._initAndRenderMaze = function(){
	var root = this._rootElem;
	var setBeginningPoint = (function(x, y){this.beginningPoint = {x:x, y:y}}).bind(this);
	var setEndPoint = (function(x, y){this.endPoint = {x:x, y:y}}).bind(this);
	this._representation.reduce(function(rootNode, rowRepresentation, yIndex){
		var row = document.createElement("div");
		row.className = "row";
		rowRepresentation.reduce(function(rowNode, cellType, xIndex){
			var cell = document.createElement("div");
			cell.className = "cell";
			switch(cellType){
				case 0: 
					setBeginningPoint(xIndex, yIndex);
					break;
				case 2:
					cell.className = cell.className + " wall";
					break;
				case 3:
					setEndPoint(xIndex, yIndex);
					cell.className = cell.className + " end";
					break;
			}
			rowNode.appendChild(cell);
			return rowNode;
		},row);
		rootNode.appendChild(row);
		return rootNode;
	}, root);
};

/*destroys the current maze display*/
Maze.prototype._clearMazeRoot = function(){
	var root = this._rootElem;
	while (root.firstChild) {
		root.removeChild(root.firstChild);
	}
};

/*inits the character position to the beginning point of the maze*/
Maze.prototype._initCharacter = function(){
	this._characterPos = this.beginningPoint ? {x: this.beginningPoint.x , y:this.beginningPoint.y} : null;
};

/*renders the character in the maze. This is done by adding a css class "character" on the cell whith the same coordinates as the one stored in the _characterPos property.
In addition, if the "character" class is present anywhere else in the maze, it will be removed.
The methods doesn't do any validity check, it just renders.
*/
Maze.prototype._renderCharacter = function(){
	var character = this._characterPos;
	var root = this._rootElem;
	if(character){
		var previousPos = root.querySelector(".character");
		if(previousPos){
			previousPos.className = previousPos.className.replace("character", "");
		}
		var characterCell = root.children[character.y].children[character.x];
		characterCell.className = characterCell.className + ' character'; 
	}
};

/*here you could make the character move */
Maze.prototype.moveCharacter = function(direction){
	this._renderCharacter(); //draws the character on screen
};

/*utility: returns true if the given coordinates can hold the character*/
Maze.prototype.isWalkableCell = function(x, y){
	/*not really useful as is...*/
	return true;
};

/*utility: returns true if the given coordinates are those of the endpoint of the maze*/
Maze.prototype.isEndPoint = function(x, y){
	/*this may be used for something...*/
	return true;
};