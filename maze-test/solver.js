function MazeSolver(maze){
	this._maze = maze;
}

/*this should return the path from current character position to end of the maze*/
MazeSolver.prototype.getPath = function(){
	return [];
};

/*this method could be used to take a path as input and move the character along it...*/
MazeSolver.prototype.moveCharacterToEnd = function(){
	return;
};